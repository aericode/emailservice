# Use the official Node.js image as the base image
FROM node:14

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the script file to the working directory
COPY emailService.js .

# Start the consumer script when the container starts
CMD ["node", "emailService.js"]
