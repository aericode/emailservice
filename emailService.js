const amqp = require('amqplib');
const nodemailer = require('nodemailer');

// RabbitMQ connection URL
const rabbitMQUrl = 'amqp://tre:123456@34.151.222.162:5672';


const transporter = nodemailer.createTransport({
	service: 'Gmail',
	auth: {
		user: 'storedevaneios@gmail.com',
		pass: 'wfqmxbmbhkdidglv'
	}
});


async function consumeMessages() {
	console.log("consumer service initialized")

	try {
		// Create a connection to RabbitMQ
		const connection = await amqp.connect(rabbitMQUrl);
		const channel = await connection.createChannel();

		// Create a queue
		const queue = 'NOTIFICACAO';
		await channel.assertQueue(queue, { durable: true });

		// Consume messages from the queue
		channel.consume(queue, async (message) => {
			const { email, text } = JSON.parse(message.content.toString());
			console.log("consumindo...")
			console.log("email: ", email)
			console.log("text: ", text)

			// Define the email options
			const mailOptions = {
				from: 'storedevaneios@gmail.com',
				to: email,
				subject: 'Notificação da Devaneios Store S.A.',
				text: text
			};

			// Send the email
			transporter.sendMail(mailOptions, (error, info) => {
				if (error) {
					console.log('Error:', error);
				} else {
					console.log('Email sent:', info.response);
				}
			});

			// Acknowledge the message
			channel.ack(message);
		});
	} catch (error) {
		console.error('Error consuming messages:', error);
	}
}

// Start consuming messages
consumeMessages();